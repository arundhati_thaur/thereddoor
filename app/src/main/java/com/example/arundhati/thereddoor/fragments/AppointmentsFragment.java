package com.example.arundhati.thereddoor.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;


import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arundhati.thereddoor.R;
import com.example.arundhati.thereddoor.activities.AppointmentsActivity;

import com.example.arundhati.thereddoor.activities.InvitationDetailsActivity;
import com.example.arundhati.thereddoor.activities.SignInActivity;
import com.example.arundhati.thereddoor.adapters.RecylcerViewAdapter;
import com.example.arundhati.thereddoor.entities.Appointment;
import com.example.arundhati.thereddoor.retrofit.RestClient;
import com.example.arundhati.thereddoor.retrofit.ServerResponse;
import com.example.arundhati.thereddoor.utils.AppConstants;
import com.example.arundhati.thereddoor.utils.OnItemClickListener;
import com.example.arundhati.thereddoor.utils.SendInvitationData;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Arundhati on 11/9/2015.
 */
public class AppointmentsFragment extends Fragment implements AppConstants,View.OnClickListener,OnItemClickListener{
    TextView header, timeToAccept;
    Button menubutton, scheduled, invitations;
    RecyclerView appointments;
    Appointment scheduledAppointments;
    ArrayList<Appointment> scheduledAppointmentList;
    RecylcerViewAdapter recylcerViewAdapter;
    public static Boolean value = false;

    SendInvitationData sendInvitationData;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.appointment_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        header = (TextView) getActivity().findViewById(R.id.headerText);
        header.setText(APPOINTMENT);
        timeToAccept = (TextView) getActivity().findViewById(R.id.timeToAccept);
        menubutton = (Button) getActivity().findViewById(R.id.backButton);
        scheduled = (Button) getActivity().findViewById(R.id.scheduled);
        invitations = (Button) getActivity().findViewById(R.id.invitations);
        scheduledAppointmentList = new ArrayList<>();
        menubutton.setBackgroundResource(R.mipmap.menu_icon_normal);
//        sendInvitationData = (SendInvitationData) getActivity();
        appointments = (RecyclerView) getActivity().findViewById(R.id.recyclerView);
        appointments.setLayoutManager(new LinearLayoutManager(getActivity()));
        recylcerViewAdapter = new RecylcerViewAdapter(scheduledAppointmentList, getActivity(), R.layout.appointment_list_layout);
        appointments.setAdapter(recylcerViewAdapter);
        for (int i = 0; i < 3; i++) {
            scheduledAppointments = new Appointment();

            scheduledAppointments.setCustomerName(String.valueOf(i));
            scheduledAppointments.setDateOfService("31,Oct,2015");
            scheduledAppointments.setTimeOfService("2:15 PM");
            scheduledAppointments.setDuration("60 min");
            scheduledAppointments.setLocation("Sector 19, Chandigarh");
            scheduledAppointments.setService("chabd");
            scheduledAppointments.setTimeToAccept("Time to accept 5:00");
            scheduledAppointmentList.add(scheduledAppointments);
            recylcerViewAdapter.notifyDataSetChanged();
        }
        scheduled.setOnClickListener(this);
        invitations.setOnClickListener( this);
        menubutton.setOnClickListener(this);


    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scheduled:
                scheduled.setBackgroundResource(R.mipmap.small_btn_pressed);
                scheduled.setTextColor(Color.WHITE);
                invitations.setBackgroundResource(R.mipmap.small_btn_normal);
                invitations.setTextColor(Color.DKGRAY);
                value = false;
                recylcerViewAdapter.notifyDataSetChanged();
                break;

            case R.id.invitations:
                invitations.setBackgroundResource(R.mipmap.small_btn_pressed);
                invitations.setTextColor(Color.WHITE);
                scheduled.setBackgroundResource(R.mipmap.small_btn_normal);
                scheduled.setTextColor(Color.DKGRAY);
                value = true;
                recylcerViewAdapter.setOnItemClickListener(this);
                recylcerViewAdapter.notifyDataSetChanged();
                break;
            case R.id.backButton:
                AppointmentsActivity.drawerLayout.openDrawer(AppointmentsActivity.drawerRelative);
                break;
        }

    }
    @Override
    public void onItemClick(int position) {
       Intent invitationDetailsIntent = new Intent(getActivity(), InvitationDetailsActivity.class);
        invitationDetailsIntent.putExtra(INVITATION_OBJECT,scheduledAppointmentList.get(position));
        startActivity(invitationDetailsIntent);
           }






}

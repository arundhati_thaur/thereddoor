package com.example.arundhati.thereddoor.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arundhati.thereddoor.retrofit.ServerResponse;
import com.example.arundhati.thereddoor.retrofit.RestClient;
import com.example.arundhati.thereddoor.utils.AppConstants;
import com.example.arundhati.thereddoor.R;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Field;

public class AboutMeActivity extends AppCompatActivity implements AppConstants, View.OnClickListener {
    TextView header, charCount;
    Button backButton, saveButton;
    EditText aboutMe;
    int typedCharCount = 0;
    SharedPreferences sharedPreferences;
    ProgressDialog dialogBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me);
        initialization();

        //text watcher to count the no characters typed by user
        TextWatcher editTextWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                typedCharCount = s.length();
            }

            public void afterTextChanged(Editable s) {
                charCount.setText(typedCharCount + "|" + MAX_LENGTH);
            }
        };
        aboutMe.addTextChangedListener(editTextWatcher);

    }

    //initialization of the UI components
    public void initialization() {
        aboutMe = (EditText) findViewById(R.id.aboutMe);
        charCount = (TextView) findViewById(R.id.charCount);
        backButton = (Button) findViewById(R.id.backButton);
        saveButton = (Button) findViewById(R.id.save);
        header = (TextView) findViewById(R.id.headerText);
        header.setText(ABOUT_ME);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        backButton.setOnClickListener(this);
        saveButton.setOnClickListener(this);
    }

    //api call to post about me on server
    void aboutMeApiCall() {
        String token = sharedPreferences.getString(ACCESS_TOKEN, NULL);
        Map<String, String> params = new HashMap<>();
        params.put("aboutMe", aboutMe.getText().toString());
        new RestClient().getService().putAboutMe(BEARER + token, params
                , new Callback<ServerResponse>() {
            @Override
            public void success(ServerResponse loginRegisterResponse, Response response) {
                dialogBox.dismiss();
                startActivity(new Intent(AboutMeActivity.this, AddressActivity.class));
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                dialogBox.dismiss();
                Toast.makeText(AboutMeActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backButton:
                super.onBackPressed();
                break;
            case R.id.save:
                String aboutMeText = aboutMe.getText().toString();

                //validate the about me text
                if ((aboutMeText != null) && (aboutMeText.length() > MIN_LENGTH)) {
                    dialogBox = ProgressDialog.show(this, DIALOG_TITLE, DIALOG_TEXT);
                    aboutMeApiCall();
                } else if (aboutMeText.length() < MIN_LENGTH) {
                    Toast.makeText(this, ABOUT_ME_TOAST_MESSAGE, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}

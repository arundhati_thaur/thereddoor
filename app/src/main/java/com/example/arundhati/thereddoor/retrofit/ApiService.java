package com.example.arundhati.thereddoor.retrofit;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.PartMap;
import retrofit.mime.TypedString;

/**
 * Created by Arundhati on 11/2/2015.
 */
public interface ApiService {

    String GET_REGIONS = "/api/agent/region";
    String POST_LOGIN = "/api/agent/login";
    String PUT_LOGOUT = "/api/agent/logout";
    String POST_REGISTER = "/api/agent/register";
    String PUT_ABOUT_ME = "/api/agent/aboutme";
    String POST_ADDRESS = "/api/agent/address";

    //api service to retrieve the regions
    @GET(GET_REGIONS)
    void getRegionName(Callback<Regions> callback);

    //api service for sign-in
    @FormUrlEncoded
    @POST(POST_LOGIN)
    void getLogin(@FieldMap Map<String,String> params
                 ,Callback<ServerResponse> callback);

    //api service for logout
    @PUT(PUT_LOGOUT)
    void logout(@Header("authorization") String authorization, Callback<ServerResponse> callback);

    //api service for sign-up
    @Multipart
    @POST(POST_REGISTER)
    void getRegister(@PartMap Map<String,TypedString> params,
                     Callback<ServerResponse> callback);

    //api service for aboutMe
    @FormUrlEncoded
    @PUT(PUT_ABOUT_ME)
    void putAboutMe(@Header("authorization") String authorization,
                    @FieldMap Map<String,String> params,
                    Callback<ServerResponse> callback);

    //api service for address
    @FormUrlEncoded
    @POST(POST_ADDRESS)
    void postAddress(@Header("authorization") String authorization,
                     @FieldMap Map<String,String> params,
                     @FieldMap Map<String,Double> latLngParams,
                     Callback<ServerResponse> callback);

}


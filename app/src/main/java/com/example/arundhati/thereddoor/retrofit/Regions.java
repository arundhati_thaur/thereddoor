package com.example.arundhati.thereddoor.retrofit;

import com.example.arundhati.thereddoor.retrofit.Region;

import java.util.ArrayList;

/**
 * Created by Arundhati on 11/3/2015.
 */
public class Regions {
    String message;
    ArrayList<Region> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Region> getData() {
        return data;
    }

}
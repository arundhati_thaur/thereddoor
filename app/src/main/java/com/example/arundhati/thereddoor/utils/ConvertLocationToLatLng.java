package com.example.arundhati.thereddoor.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Arundhati on 10/30/2015.
 */
public class ConvertLocationToLatLng implements AppConstants{
    public static void getLatLng(final String customerLocation, final Context context, final Handler handler)
    {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                double[] latLng= new double[2];
                try {
                    List customerAddress = geocoder.getFromLocationName(customerLocation, SECOND);
                    if (customerAddress != null && customerAddress.size() > FIRST) {
                        Address address = (Address) customerAddress.get(FIRST);
                        latLng[FIRST]=address.getLatitude();
                        latLng[SECOND]=address.getLongitude();

                    }
                } catch (IOException e) {
                   e.printStackTrace();
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    message.what=LAT_LNG_RECEIVED;
                    Bundle bundle = new Bundle();
                    bundle.putDoubleArray(LAT_LNG_BUNDLE,latLng);
                    message.setData(bundle);
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }
    public static void getAddressDetails(final String address,final Context context,final Handler handler)
    {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                double[] latLng= new double[2];
                String zip=NULL, city=NULL, street=NULL, state=NULL, appartment=NULL;
                try {
                    List customerAddress = geocoder.getFromLocationName(address, 1);
                    if (customerAddress != null && customerAddress.size() > FIRST) {
                        Address agentAddress = (Address) customerAddress.get(FIRST);
                        zip = agentAddress.getPostalCode();
                        city =agentAddress.getLocality() ;
                        street =agentAddress.getFeatureName();
                        state =agentAddress.getAdminArea();
                        appartment =agentAddress.getSubLocality();
                        latLng[FIRST] = agentAddress.getLatitude();
                        latLng[SECOND] = agentAddress.getLongitude();


                    }
                } catch (IOException e) {
                   e.printStackTrace();
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    message.what=ADDRESS_RECEIVED;
                    Bundle bundle = new Bundle();
                    bundle.putString(ZIP, zip);
                    bundle.putString(CITY, city);
                    bundle.putString(STATE, state);
                    bundle.putString(STREET, street);
                    bundle.putString(APPARTMENT, appartment);
                    bundle.putDoubleArray(ADDRESS_LAT_LNG_BUNDLE, latLng);
                    message.setData(bundle);
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }

}

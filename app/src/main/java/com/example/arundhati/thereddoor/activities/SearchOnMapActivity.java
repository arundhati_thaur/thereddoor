package com.example.arundhati.thereddoor.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arundhati.thereddoor.R;
import com.example.arundhati.thereddoor.utils.AppConstants;
import com.example.arundhati.thereddoor.adapters.SelectLocationAdapter;
import com.example.arundhati.thereddoor.utils.ConvertLocationToLatLng;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class SearchOnMapActivity extends FragmentActivity implements
        OnMapReadyCallback, View.OnClickListener, AppConstants, AdapterView.OnItemClickListener, GoogleMap.OnMyLocationChangeListener {
    TextView header;
    EditText chooseLocation;
    Button done, backButton;
    GoogleMap googleMap;
    boolean locationSelected;
    AutoCompleteTextView autoCompView;
    double[] latlng;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        initialization();
        done.setOnClickListener(this);
        backButton.setOnClickListener(this);
        autoCompView.setAdapter(new SelectLocationAdapter(this, R.layout.auto_complete_text_view));
        autoCompView.setOnItemClickListener(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        latlng = new double[2];
        googleMap = mapFragment.getMap();
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        //check if the google play services are available
        if (status != ConnectionResult.SUCCESS) {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            googleMap = fm.getMap();
            googleMap.setMyLocationEnabled(true);
            googleMap.setOnMyLocationChangeListener(this);
        }
        //change the background image of My Location Button
        ImageView locationButton = (ImageView) ((View) mapFragment.getView().findViewById(1).getParent()).findViewById(2);
        locationButton.setImageResource(R.drawable.selector_current_location);

    }

    //initialization of the UI components
    public void initialization()
    {
        header = (TextView) findViewById(R.id.headerText);
        chooseLocation = (EditText) findViewById(R.id.chooseLocation);
        done = (Button) findViewById(R.id.done);
        backButton = (Button) findViewById(R.id.backButton);
        autoCompView = (AutoCompleteTextView) findViewById(R.id.chooseLocation);
        header.setText(SELECT_LOCATION);
    }

    //add marker on location selected by the user
    private void setLocation() {
        googleMap.clear();
        LatLng latLng = new LatLng(latlng[0], latlng[1]);
        googleMap.addMarker(new MarkerOptions().position(latLng).anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromResource(R.mipmap.your_location_icon)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    //check if google play services are available
    public boolean googleMapAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status)
            return true;
        else
            return false;
    }

    //handler class to retrieve the bundle message containing latitude and longitude
    private class LatLngHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case LAT_LNG_RECEIVED:
                    Bundle bundle = msg.getData();
                    latlng = bundle.getDoubleArray(LAT_LNG_BUNDLE);
                    locationSelected = true;
                    setLocation();
                    break;
                default:
                    Toast.makeText(SearchOnMapActivity.this, ADDRESS_NOT_FOUND, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backButton:
                super.onBackPressed();
                break;

            case R.id.done:
                Intent intent = new Intent();
                intent.putExtra(ADDRESS_ID, autoCompView.getText().toString());
                setResult(RESULT_OK, intent);
                finish();


                break;
        }

    }

    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        String agentAddress = (String) adapterView.getItemAtPosition(position);
        if (!googleMapAvailable()) {
            Toast.makeText(SearchOnMapActivity.this, MSG_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();

        } else {

            ConvertLocationToLatLng convertLocationToLatLng = new ConvertLocationToLatLng();
            convertLocationToLatLng.getLatLng(agentAddress, getApplicationContext(), new LatLngHandler());
            hideKeybord();
        }
        Toast.makeText(this, agentAddress, Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onMyLocationChange(Location location) {
        //focus camera on the current location if no location is selected by user
        if (!locationSelected) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

        }
    }

    public void hideKeybord() {
        //to hide keyboard if shown
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isActive())
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

}
package com.example.arundhati.thereddoor.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.arundhati.thereddoor.fragments.AppointmentsFragment;
import com.example.arundhati.thereddoor.R;
import com.example.arundhati.thereddoor.activities.CustomerLocationOnMapActivity;
import com.example.arundhati.thereddoor.entities.Appointment;
import com.example.arundhati.thereddoor.utils.AppConstants;
import com.example.arundhati.thereddoor.utils.OnItemClickListener;

import java.util.ArrayList;

/**
 * Created by Arundhati on 10/27/2015.
 */
public class RecylcerViewAdapter extends RecyclerView.Adapter<RecylcerViewAdapter.Holder> implements AppConstants{
    ArrayList<Appointment> data;
    Context context;
    int rowLayout;
    OnItemClickListener onItemClickListener;


    public RecylcerViewAdapter(ArrayList<Appointment> data, Context context, int rowLayout) {
        this.data = data;
        this.context = context;
        this.rowLayout = rowLayout;
    }

    @Override
    public RecylcerViewAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(rowLayout, null);
        Holder viewHolder = new Holder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecylcerViewAdapter.Holder holder, final int position) {

        if(AppointmentsFragment.value == false) {
            holder.customerName.setText(data.get(position).getCustomerName());
            holder.service.setText(data.get(position).getService());
            holder.location.setText(data.get(position).getLocation());
            holder.duration.setText(data.get(position).getDuration());
            holder.timeToAccept.setText(NULL);
            holder.dateAndTimeOfService.setText(data.get(position).getTimeOfService() + "," + data.get(position).getDateOfService());
            holder.locationOnMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent customerLocationIntent = new Intent(context, CustomerLocationOnMapActivity.class);
                    customerLocationIntent.putExtra(LOCATION_ID,data.get(position).getLocation());
                    context.startActivity(customerLocationIntent);
                }
            });
            holder.appointmentListContainer.setOnClickListener(null);
            }else {
            holder.customerName.setText(data.get(position).getCustomerName());
            holder.service.setText(data.get(position).getService());
            holder.location.setText(data.get(position).getLocation());
            holder.duration.setText(data.get(position).getDuration());
            holder.timeToAccept.setText(data.get(position).getTimeToAccept());
            holder.dateAndTimeOfService.setText(data.get(position).getTimeOfService() + "," + data.get(position).getDateOfService());
            holder.appointmentListContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(position);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }
    public class Holder extends RecyclerView.ViewHolder {
        TextView customerName,service,location,duration,dateAndTimeOfService,timeToAccept;
        ImageButton locationOnMap;
        RelativeLayout appointmentListContainer;

        public Holder(View view) {
            super(view);
            timeToAccept = (TextView) view.findViewById(R.id.timeToAccept);
            appointmentListContainer = (RelativeLayout)view.findViewById(R.id.appointmentListContainer);
            customerName = (TextView)view.findViewById(R.id.customerName);
            service = (TextView)view.findViewById(R.id.service);
            location = (TextView)view.findViewById(R.id.location);
            duration = (TextView)view.findViewById(R.id.duration);
            dateAndTimeOfService = (TextView)view.findViewById(R.id.dateAndTime);
            locationOnMap = (ImageButton)view.findViewById(R.id.locationOnMap);

        }
    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}

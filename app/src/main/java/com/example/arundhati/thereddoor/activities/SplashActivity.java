package com.example.arundhati.thereddoor.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.arundhati.thereddoor.utils.AppConstants;
import com.example.arundhati.thereddoor.R;
import com.google.android.gms.iid.InstanceID;


public class SplashActivity extends AppCompatActivity implements AppConstants {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new GetDeviceToken().execute();


    }

    //background task to get device token from server
    private class GetDeviceToken extends AsyncTask<Void, Void, String> {

        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(SplashActivity.this,DIALOG_TITLE,DIALOG_TEXT);
            

        }

        @Override
        protected String doInBackground(Void... params) {

            String token = null;
            try {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
                int returnVal = p1.waitFor();
                boolean reachable = (returnVal == 0);
                if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting() && reachable) {
                    token = InstanceID.getInstance(SplashActivity.this).getToken(PROJECT_NUMBER, SCOPES);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return token;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            if (s != null && !s.isEmpty()) {
                Toast.makeText(SplashActivity.this, s, Toast.LENGTH_SHORT).show();
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
                SharedPreferences.Editor editor = preferences.edit();
                // Edit the saved preferences
                editor.putString(DEVICE_TOKEN, s);
                editor.commit();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        SplashActivity.this.startActivity(new Intent(SplashActivity.this, SignInActivity.class));
                        SplashActivity.this.finish();
                    }
                }, SLEEP_SPLASH);

            } else {
                new AlertDialog.Builder(SplashActivity.this).setTitle(MSG_NO_INTERNET_CONNECTION).
                        setMessage(RETRY).
                        setPositiveButton(YES, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new GetDeviceToken().execute();
                            }
                        }).
                        setNegativeButton(NO,
                                new android.content.DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        SplashActivity.this.finish();
                                    }
                                }).setCancelable(false)
                        .create().show();
            }
        }
    }
}
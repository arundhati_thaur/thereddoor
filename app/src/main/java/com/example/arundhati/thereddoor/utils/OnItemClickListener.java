package com.example.arundhati.thereddoor.utils;

/**
 * Created by Arundhati on 10/27/2015.
 */
public interface OnItemClickListener {
    void onItemClick(int position);
}

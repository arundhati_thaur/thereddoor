package com.example.arundhati.thereddoor.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arundhati.thereddoor.adapters.GenderSpinnerAdapter;
import com.example.arundhati.thereddoor.adapters.RegionSpinnerAdapter;
import com.example.arundhati.thereddoor.retrofit.Region;
import com.example.arundhati.thereddoor.retrofit.ServerResponse;
import com.example.arundhati.thereddoor.utils.AppConstants;
import com.example.arundhati.thereddoor.R;
import com.example.arundhati.thereddoor.retrofit.Regions;
import com.example.arundhati.thereddoor.retrofit.RestClient;
import com.example.arundhati.thereddoor.utils.Validations;

import retrofit.client.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.http.Part;
import retrofit.mime.TypedString;

public class SignUpActivity extends AppCompatActivity implements AppConstants, View.OnClickListener {
    TextView signUpText, alreadyMember, terms;
    EditText firstName, lastName, password, confirmPassword, emailAddress, registeredId, contact;
    Button backButton, signUpButton;
    GenderSpinnerAdapter adapterSpinner;
    Spinner gender, region;
    ArrayList<Region> regionList;
    ScrollView signUpScroll;
    RegionSpinnerAdapter regionSpinnerAdapter;
    SharedPreferences sharedPreferences;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initialization();
        regionsApiCall();


    }
    //initialization of the UI components
    public void initialization() {
        regionList = new ArrayList<>();
        backButton = (Button) findViewById(R.id.backButton);
        contact = (EditText) findViewById(R.id.contact);
        signUpButton = (Button) findViewById(R.id.signUpButton);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        password = (EditText) findViewById(R.id.password);
        region = (Spinner) findViewById(R.id.regionSpinner);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        emailAddress = (EditText) findViewById(R.id.email);
        registeredId = (EditText) findViewById(R.id.registeredID);
        alreadyMember = (TextView) findViewById(R.id.alreadyMeamber);
        terms = (TextView) findViewById(R.id.terms);
        signUpScroll = (ScrollView) findViewById(R.id.signUpScroll);
        gender = (Spinner) findViewById(R.id.spinner);
        signUpText = (TextView) findViewById(R.id.headerText);
        signUpText.setText(R.string.text_signUp);
        signUpScroll.setVerticalScrollBarEnabled(false);
        String alreadyMemberText = "<font color=#ABACAD>Already a Member? </font>" +
                " <font color=#B9252B><u>SIGN IN</u></font>" +
                "<font color=#ABACAD> here</font>";
        alreadyMember.setText(Html.fromHtml(alreadyMemberText));
        String termsText = "<font color=#ABACAD>You agree to  </font>" +
                " <font color=#B9252B><u>Terms of Service and Policy</u></font>";
        adapterSpinner = new GenderSpinnerAdapter(this,getResources().getStringArray(R.array.spinnerList));
        gender.setAdapter(adapterSpinner);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        regionSpinnerAdapter = new RegionSpinnerAdapter(this, regionList);
        region.setAdapter(regionSpinnerAdapter);
        terms.setText(Html.fromHtml(termsText));
        backButton.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        alreadyMember.setOnClickListener(this);
        terms.setOnClickListener(this);
        Region regionhint = new Region();
        regionhint.setRegionName(REGION_HINT);
        regionList.add(regionhint);

    }

    //api call to retrive region list
    public void regionsApiCall() {
        new RestClient().getService().getRegionName(new Callback<Regions>() {


            @Override
            public void success(Regions regions, Response response) {

                regionList.addAll(regions.getData());
                regionSpinnerAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(SignUpActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //api call for sign up
    public void registerApiCall() {
        Map<String,TypedString> params = new HashMap<>();
        params.put("firstName",new TypedString(firstName.getText().toString()));
        params.put("lastName",new TypedString(lastName.getText().toString()));
        params.put("emailId",new TypedString(emailAddress.getText().toString()));
        params.put("employeeId",new TypedString(registeredId.getText().toString()));
        params.put("region",new TypedString(region.getSelectedItem().toString()));
        params.put("password",new TypedString(password.getText().toString()));
        params.put("phoneNumber",new TypedString(contact.getText().toString()));
        params.put("deviceType",new TypedString(DEVICE_TYPE));
        params.put("deviceToken",new TypedString(DEVICE_TOKEN));
        params.put("gender",new TypedString(gender.getSelectedItem().toString()));
        new RestClient().getService().getRegister(params, new Callback<ServerResponse>() {
                    @Override
                    public void success(ServerResponse loginRegisterResponse, Response response) {
                        Toast.makeText(SignUpActivity.this,SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show();
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(ACCESS_TOKEN, loginRegisterResponse.getData().getToken());
                        editor.putString(FIRST_NAME,loginRegisterResponse.getData().getAgent().getName().getFirstName());
                        editor.commit();
                        dialog.dismiss();

                        Intent aboutMeIntent = new Intent(SignUpActivity.this, AboutMeActivity.class);
                        startActivity(aboutMeIntent);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        dialog.dismiss();
                        Toast.makeText(SignUpActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backButton:
                super.onBackPressed();
                break;
            case R.id.signUpButton:
                Boolean validFirstName = Validations.validateFirstName(firstName.getText().toString());
                Boolean validLastName = Validations.validateLastName(lastName.getText().toString());
                Boolean validEmail = Validations.validateEmail(emailAddress.getText().toString());
                Boolean validPassword = Validations.validatePassword(password.getText().toString());
                Boolean validConfirmPassword = Validations.validateConfirmPassword(password.getText().toString(), confirmPassword.getText().toString());
               //valodate the data entered by the user
                if ((validFirstName == VALID) && (validLastName == VALID) && (validEmail == VALID)
                        && (validPassword == VALID) && (validConfirmPassword == VALID)
                        && (contact.getText().toString().length() == MAX_LENGTH_CONTACT)&&(registeredId.getText().toString().length() >= MIN_LENGTH_ID)) {
                    dialog = ProgressDialog.show(this,DIALOG_TITLE,DIALOG_TEXT);
                    registerApiCall();
                } else if (validFirstName == INVALID) {
                    Toast.makeText(this, INVALID_FNAME, Toast.LENGTH_SHORT).show();
                } else if (validLastName == INVALID) {
                    Toast.makeText(this, INVALID_LNAME, Toast.LENGTH_SHORT).show();
                } else if (validEmail == INVALID) {
                    Toast.makeText(this, INVALID_EMAIL, Toast.LENGTH_SHORT).show();
                } else if (validPassword == INVALID) {
                    Toast.makeText(this, INVALID_PASSWORD, Toast.LENGTH_SHORT).show();
                } else if (validConfirmPassword == INVALID) {
                    Toast.makeText(this, PASSWORD_DOSENT_MATCH, Toast.LENGTH_SHORT).show();
                } else if (contact.getText().toString().length() < MAX_LENGTH_CONTACT) {
                    Toast.makeText(this, INVALID_CONTACT, Toast.LENGTH_SHORT).show();
                }else if (registeredId.getText().toString().length() < MIN_LENGTH_ID) {
                    Toast.makeText(this, INVALID_EMPLOYEE_ID, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.alreadyMeamber:
                Intent signInIntent = new Intent(this, SignInActivity.class);
                startActivity(signInIntent);
                finish();
                break;
            case R.id.terms:
                break;
        }

    }
}

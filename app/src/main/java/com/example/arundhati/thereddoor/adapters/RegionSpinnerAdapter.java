package com.example.arundhati.thereddoor.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.arundhati.thereddoor.R;
import com.example.arundhati.thereddoor.retrofit.Region;
import com.example.arundhati.thereddoor.utils.AppConstants;

import java.util.ArrayList;

/**
 * Created by Arundhati on 11/3/2015.
 */
public class RegionSpinnerAdapter extends BaseAdapter implements AppConstants {
    Context context;
    ArrayList<Region> regionList;

    public RegionSpinnerAdapter(Context context, ArrayList regionList) {

        this.context = context;
        this.regionList = regionList;
    }



    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view;
        if (position == FIRST) {
            view = inflater.inflate(R.layout.hidden_layout, parent, false); // Hide first row
        } else {
            view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText(regionList.get(position).getRegionName());
            textView.setTextColor(Color.BLACK);
        }

        return view;
    }


    @Override
    public int getCount() {
        return regionList == null ? 0 : regionList.size();
    }

    @Override
    public Object getItem(int position) {
        return regionList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = View.inflate(context, android.R.layout.simple_list_item_1, null);
        TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
        textView.setText(regionList.get(position).getRegionName().toString());
        return convertView;
    }
}

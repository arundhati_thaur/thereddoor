package com.example.arundhati.thereddoor.utils;

/**
 * Created by Arundhati on 10/23/2015.
 */
public interface AppConstants {
    //
    int SLEEP_SPLASH = 2000;

    // text for various textViews
    String ABOUT_ME = "ABOUT ME";
    String ADDRESS = "ADDRESS";
    String APPOINTMENT = "APPOINTMENTS";
    String INVITATION_DETAILS = "INVITATION DETSAILS";
    String CUSTOMER_LOCATION = "CUSTOMER LOCATION";
    String SELECT_LOCATION = "SELECT LOCATION";
    String LOCATION_TEXT = "SERVICE AT";
    String REGION_HINT = "Regions";

    // max and min length for about me textView
    int MAX_LENGTH = 140;
    int MIN_LENGTH = 20;

    // navigation drawer items
    int HOME = 0;
    int LOGOUT_BUTTON = 6;

    // Google places autocomplete
    String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    String TYPE_AUTOCOMPLETE = "/autocomplete";
    String OUT_JSON = "/json";
    String PLACES_KEY = "AIzaSyA67q5AuS43hQNuuxNrhuHIqNEmnZ7LzMk";

    //Validations
    Boolean VALID = true;
    Boolean INVALID = false;
    String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z]).{6,})";
    String FIRST_NAME_PATTERN = "([a-zA-Z]*.{3,})";
    String LAST_NAME_PATTERN = "([a-zA-z]*.{3,})";
    int MAX_LENGTH_CONTACT = 10;
    int MIN_LENGTH_ID = 3;

    //request code for search on map activity
    int SEARCH_ON_MAP_REQUEST_CODE = 1;

    //toast messages
    String MSG_NO_INTERNET_CONNECTION = "Check Internet connection";
    String ABOUT_ME_TOAST_MESSAGE = "Minimum 20 characters required";
    String SUCCESS_MESSAGE = "Success";
    String INVALID_FNAME = "Invalid First Name";
    String INVALID_LNAME = "Invalid Last Name";
    String INVALID_EMAIL = "Invalid Email";
    String INVALID_EMPLOYEE_ID = "Invalid ID";
    String INVALID_PASSWORD = "Invalid Password";
    String PASSWORD_DOSENT_MATCH = "Password doesn't match";
    String INVALID_CONTACT = "Invalid Phone Number";
    String ADDRESS_TOAST = "All Fields are required";
    String ADDRESS_NOT_FOUND = "Unable to find address";

    //shared preferences
    String FIRST_NAME = "first_name";
    String DEVICE_TOKEN = "device Token",
            DEVICE_TYPE = "ANDROID",
            ACCESS_TOKEN = "Access Token",
            PROJECT_NUMBER = "735761420460";

    String SCOPES = "GCM";
    String ZIP = "zip",
            CITY = "city",
            STATE = "state",
            STREET = "street",
            APPARTMENT = "appartment";
    //Dialog box text
    String DIALOG_TITLE = "Processing";
    String DIALOG_TEXT = "Wait !";


    String BEARER = "Bearer ";
    Double NULL_LAT_LNG = 0.0;
    String ADDRESS_ID ="address";
    String LOCATION_ID = "location";
    String ADDRESS_LAT_LNG_BUNDLE = "AddressLatLng";
    String LAT_LNG_BUNDLE = "LatLng";
    int ADDRESS_RECEIVED = 1;
    int LAT_LNG_RECEIVED = 1;
    int FIRST = 0;
    int SECOND = 1;
    String NULL = "";

    //Alert dialog buttons
    String YES = "Yes";
    String NO = "No";
    String RETRY = "Retry";

    String INVITATION_OBJECT  = "invitationObject";
}

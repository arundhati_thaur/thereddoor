package com.example.arundhati.thereddoor.retrofit;

/**
 * Created by Arundhati on 11/4/2015.
 */
public class ServerResponse {
    String statusCode, message;
    Values data;

    public  Values getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public class Values
    {
        String token;
        Agent agent;

        public Agent getAgent() {
            return agent;
        }

        public String getToken() {
            return token;
        }

        public class Agent {
            String _id, emailId, employeeId, region, phoneNumber, gender, accessToken, __v,
                    profilePicURL, aboutMe, registrationDate;

            public String get__v() {
                return __v;
            }

            public String get_id() {
                return _id;
            }

            public String getAboutMe() {
                return aboutMe;
            }

            public String getAccessToken() {
                return accessToken;
            }

            public String getEmailId() {
                return emailId;
            }

            public String getEmployeeId() {
                return employeeId;
            }

            public String getGender() {
                return gender;
            }

            public Name getName() {
                return name;
            }

            public String getPhoneNumber() {
                return phoneNumber;
            }

            public String getProfilePicURL() {
                return profilePicURL;
            }

            public String getRegion() {
                return region;
            }

            public String getRegistrationDate() {
                return registrationDate;
            }

            Name name;

            public class Name {
                String firstName, lastName;

                public String getFirstName() {
                    return firstName;
                }

                public String getLastName() {
                    return lastName;
                }
            }
        }
    }


}
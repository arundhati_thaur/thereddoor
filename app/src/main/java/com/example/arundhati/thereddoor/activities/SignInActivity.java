package com.example.arundhati.thereddoor.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arundhati.thereddoor.R;
import com.example.arundhati.thereddoor.retrofit.ServerResponse;
import com.example.arundhati.thereddoor.retrofit.RestClient;
import com.example.arundhati.thereddoor.utils.AppConstants;
import com.example.arundhati.thereddoor.utils.Validations;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Field;


public class SignInActivity extends AppCompatActivity implements View.OnClickListener, AppConstants {

    TextView notRegistered, signInText;
    EditText emailAddress, password;
    Button signIn, backButton, forgotPassword;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        initialization();
    }

    //initialization of the UI components
    void initialization() {
        signInText = (TextView) findViewById(R.id.headerText);
        backButton = (Button) findViewById(R.id.backButton);
        emailAddress = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        notRegistered = (TextView) findViewById(R.id.signUp);
        signIn = (Button) findViewById(R.id.signInButton);
        forgotPassword = (Button) findViewById(R.id.forgot);
        signInText.setText(R.string.text_signIn);
        String text = "<font color=#ABACAD>Not Registered yet? </font>" +
                " <font color=#CD2E44><u>SIGN UP</u></font>" +
                "<font color=#ABACAD> here</font>";
        notRegistered.setText(Html.fromHtml(text));
        signIn.setOnClickListener(this);
        backButton.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);
        notRegistered.setOnClickListener(this);

    }

    //api call for Sign In
    public void loginApiCall() {
        Map<String,String> params = new HashMap<>();
        params.put("emailId",emailAddress.getText().toString());
        params.put("password",password.getText().toString());
        params.put("deviceType",DEVICE_TYPE);
        params.put("deviceToken",DEVICE_TOKEN);
        new RestClient().getService().getLogin
                (params, new Callback<ServerResponse>() {
            @Override
            public void success(ServerResponse loginRegisterResponse, Response response) {
                Toast.makeText(SignInActivity.this, SUCCESS_MESSAGE , Toast.LENGTH_SHORT).show();
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(SignInActivity.this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(ACCESS_TOKEN, loginRegisterResponse.getData().getToken());
                editor.putString(FIRST_NAME,loginRegisterResponse.getData().getAgent().getName().getFirstName());
                editor.commit();
                progressDialog.dismiss();
                Intent appointmentIntent = new Intent(SignInActivity.this, AppointmentsActivity.class);
                startActivity(appointmentIntent);
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                Toast.makeText(SignInActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backButton:
                ActivityCompat.finishAffinity(this);
                break;
            case R.id.signInButton:

                Boolean validEmail = Validations.validateEmail(emailAddress.getText().toString());
                Boolean validPassword = Validations.validatePassword(password.getText().toString());
                //check if the data entered is valid
                if ((validEmail == VALID) && (validPassword == VALID)) {
                    progressDialog = ProgressDialog.show(SignInActivity.this,DIALOG_TITLE,DIALOG_TEXT);
                   loginApiCall();

                } else if (validEmail == INVALID) {
                    Toast.makeText(getApplicationContext(), INVALID_EMAIL, Toast.LENGTH_SHORT).show();

                } else if (validPassword == INVALID) {
                    Toast.makeText(getApplicationContext(), INVALID_PASSWORD, Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.signUp:
                //start the sign up activity
                Intent signUpIntent = new Intent(this, SignUpActivity.class);
                startActivity(signUpIntent);
                break;
            case R.id.forgot:
                break;
        }
    }
}

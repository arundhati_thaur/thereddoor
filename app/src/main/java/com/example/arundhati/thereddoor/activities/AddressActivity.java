package com.example.arundhati.thereddoor.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arundhati.thereddoor.R;
import com.example.arundhati.thereddoor.retrofit.ServerResponse;
import com.example.arundhati.thereddoor.retrofit.RestClient;
import com.example.arundhati.thereddoor.utils.AppConstants;
import com.example.arundhati.thereddoor.utils.ConvertLocationToLatLng;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AddressActivity extends AppCompatActivity implements View.OnClickListener, AppConstants {
    TextView headerText;
    EditText street, apartment, city, state, zip;
    Button backButton, save;
    ScrollView scrollView;
    RelativeLayout searchOnMap;
    double latitude, longitude;
    ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        initialization();
    }

    // initialization of the UI components
    public void initialization() {
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        headerText = (TextView) findViewById(R.id.headerText);
        street = (EditText) findViewById(R.id.street);
        apartment = (EditText) findViewById(R.id.apartment);
        city = (EditText) findViewById(R.id.city);
        state = (EditText) findViewById(R.id.state);
        zip = (EditText) findViewById(R.id.zip);
        backButton = (Button) findViewById(R.id.backButton);
        searchOnMap = (RelativeLayout) findViewById(R.id.searchOnMap);
        save = (Button) findViewById(R.id.save);
        headerText.setText(ADDRESS);
        scrollView.setVerticalScrollBarEnabled(false);
        backButton.setOnClickListener(this);
        searchOnMap.setOnClickListener(this);
        save.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backButton:
                super.onBackPressed();
                break;
            case R.id.searchOnMap:
                Intent mapIntent = new Intent(this, SearchOnMapActivity.class);
                startActivityForResult(mapIntent, SEARCH_ON_MAP_REQUEST_CODE);

                break;
            case R.id.save:
                String zipCode = zip.getText().toString();
                String cityName = city.getText().toString();
                String streetAddress = street.getText().toString();
                String stateName = state.getText().toString();
                String apartmentNo = apartment.getText().toString();

                //validate the address fields
                if (zipCode != null && !zipCode.isEmpty()
                        && cityName != null && !cityName.isEmpty()
                        && streetAddress != null && !streetAddress.isEmpty()
                        && stateName != null && !stateName.isEmpty()
                        && apartmentNo != null && !apartmentNo.isEmpty()) {
                    if (latitude == NULL_LAT_LNG || longitude == NULL_LAT_LNG) {
                        String agentAddress = apartmentNo + NULL + streetAddress + NULL + cityName + NULL + stateName;
                        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
                        if (status != ConnectionResult.SUCCESS) {
                            Toast.makeText(AddressActivity.this, MSG_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();
                        } else {
                            ConvertLocationToLatLng convertLocationToLatLng = new ConvertLocationToLatLng();
                            convertLocationToLatLng.getLatLng(agentAddress, getApplicationContext(), new LatLngHandler());
                        }
                    } else {
                        dialog = ProgressDialog.show(this, DIALOG_TITLE, DIALOG_TEXT);
                        addressApiCall();
                    }
                } else
                    Toast.makeText(AddressActivity.this, ADDRESS_TOAST, Toast.LENGTH_SHORT).show();


                break;
        }


    }
// function to check if the google maps services are available
    public boolean googleMapAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status)
            return true;
        else
            return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == SEARCH_ON_MAP_REQUEST_CODE) && (resultCode == RESULT_OK)) {
            String agentAddress = data.getStringExtra(ADDRESS_ID);
            if (!googleMapAvailable()) {
                Toast.makeText(AddressActivity.this, MSG_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();

            } else {

                ConvertLocationToLatLng convertLocationToLatLng = new ConvertLocationToLatLng();
                convertLocationToLatLng.getAddressDetails(agentAddress, getApplicationContext(), new AddressDetailsHandler());
            }
        }
    }
    //handler class to retrieve the bundle message containing agent address
    class AddressDetailsHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ADDRESS_RECEIVED:
                    Bundle bundle = msg.getData();
                    city.setText(bundle.getString(CITY));
                    zip.setText(bundle.getString(ZIP));
                    state.setText(bundle.getString(STATE));
                    street.setText(bundle.getString(STREET));
                    double[] latLong;
                    latLong = bundle.getDoubleArray(ADDRESS_LAT_LNG_BUNDLE);
                    latitude = latLong[0];
                    longitude = latLong[1];
                    break;
            }
        }
    }
    //api call to post address on to the server
    public void addressApiCall() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String token = sharedPreferences.getString(ACCESS_TOKEN, NULL);
        Map<String, String> params = new HashMap<>();
        Map<String, Double> latLngParams = new HashMap<>();
        params.put("streetAddress", street.getText().toString());
        params.put("apartment", apartment.getText().toString());
        params.put("city", city.getText().toString());
        params.put("state", state.getText().toString());
        params.put("zip", zip.getText().toString());
        latLngParams.put("latitude", latitude);
        latLngParams.put("longitude", longitude);
        new RestClient().getService().postAddress(BEARER + token, params, latLngParams
                , new Callback<ServerResponse>() {
            @Override
            public void success(ServerResponse loginRegisterResponse, Response response) {
                dialog.dismiss();
                startActivity(new Intent(AddressActivity.this, AppointmentsActivity.class));
                finish();

            }

            @Override
            public void failure(RetrofitError error) {
                dialog.dismiss();
                Toast.makeText(AddressActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    //handler class to retrieve the bundle message containing latitude and longitude
    private class LatLngHandler extends android.os.Handler {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();
            double[] latLong = bundle.getDoubleArray(LAT_LNG_BUNDLE);

            switch (msg.what) {
                case 1:
                    latitude = latLong[0];
                    longitude = latLong[1];
                    dialog = ProgressDialog.show(AddressActivity.this, DIALOG_TITLE, DIALOG_TEXT);
                    addressApiCall();
                    break;

            }
        }
    }
}

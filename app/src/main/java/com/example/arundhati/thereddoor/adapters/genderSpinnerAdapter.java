package com.example.arundhati.thereddoor.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.arundhati.thereddoor.R;
import com.example.arundhati.thereddoor.utils.AppConstants;

/**
 * Created by Arundhati on 11/4/2015.
 */
public class GenderSpinnerAdapter extends BaseAdapter implements AppConstants {
    Context context;
    String[] genderList;

    public GenderSpinnerAdapter(Context context, String[] genderList) {

        this.context = context;
        this.genderList = genderList;
    }


    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view;
        if (position == FIRST) {
            view = inflater.inflate(R.layout.hidden_layout, parent, false); // Hide first row
        } else {
            view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
                textView.setText(genderList[position]);
                textView.setTextColor(Color.BLACK);

        }

        return view;
    }


    @Override
    public int getCount() {
        return genderList == null ? 0 : genderList.length;
    }

    @Override
    public Object getItem(int position) {
        return genderList[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = View.inflate(context, android.R.layout.simple_list_item_1, null);
        TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
        textView.setText(genderList[position]);
        return convertView;
    }
}

package com.example.arundhati.thereddoor.activities;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arundhati.thereddoor.R;
import com.example.arundhati.thereddoor.utils.AppConstants;
import com.example.arundhati.thereddoor.utils.ConvertLocationToLatLng;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class CustomerLocationOnMapActivity extends FragmentActivity implements OnMapReadyCallback,AppConstants, View.OnClickListener {
    TextView header;
    Button backButton;
    double[] latlng = new double[2];
    GoogleMap googleMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_location_on_map);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        googleMap=mapFragment.getMap();
        header = (TextView)findViewById(R.id.headerText);
        header.setText(CUSTOMER_LOCATION);
        backButton = (Button)findViewById(R.id.backButton);
        backButton.setOnClickListener(this);

        if(!googleMapAvailable())
        {
            Toast.makeText(this, MSG_NO_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();

        }
        else
            {
                String customerLocation = getIntent().getStringExtra(LOCATION_ID);
                ConvertLocationToLatLng convertLocationToLatLng = new ConvertLocationToLatLng();
                convertLocationToLatLng.getLatLng(customerLocation,getApplicationContext(),new LatLngHandler());
            }
        }


    @Override
    public void onMapReady(GoogleMap map) {

    }
    //add marker on the location of the customer
    private void setLocation() {
        LatLng latLng = new LatLng(latlng[0], latlng[1]);
        googleMap.addMarker(new MarkerOptions().position(latLng));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }
    @Override
    public void onClick(View v) {
        super.onBackPressed();
    }

    //function to check if google play services are available
    public boolean googleMapAvailable()
    {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if(ConnectionResult.SUCCESS == status)
            return true;
        else
            return false;
    }
    // handler class to retrieve bundle message containing latitude and longitude of customer address
    private class LatLngHandler extends android.os.Handler{
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case LAT_LNG_RECEIVED:
                    Bundle bundle = msg.getData();
                    latlng = bundle.getDoubleArray(LAT_LNG_BUNDLE);
                    setLocation();
                    break;
                default:
                    Toast.makeText(CustomerLocationOnMapActivity.this, ADDRESS_NOT_FOUND, Toast.LENGTH_SHORT).show();
            }
        }
    }
}

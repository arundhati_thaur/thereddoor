package com.example.arundhati.thereddoor.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arundhati.thereddoor.fragments.AppointmentsFragment;
import com.example.arundhati.thereddoor.R;
import com.example.arundhati.thereddoor.retrofit.ServerResponse;
import com.example.arundhati.thereddoor.retrofit.RestClient;
import com.example.arundhati.thereddoor.utils.AppConstants;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AppointmentsActivity extends AppCompatActivity implements AppConstants, AdapterView.OnItemClickListener {


Fragment appointmentFragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
   public static DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    ListView drawerList;
   SharedPreferences sharedPreferences;
    ArrayAdapter navDrawerAdapter;
   public static RelativeLayout drawerRelative;
    ProgressDialog progressDialog;
    TextView agentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointments);
        initialization();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        //initializing the navigation drawer
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {

            }

            public void onDrawerOpened(View drawerView) {

                String firstName = sharedPreferences.getString(FIRST_NAME, "");
              agentName.setText(firstName);

            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(drawerToggle);

        navDrawerAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.navDrawerList));
        drawerList.setAdapter(navDrawerAdapter);
        navDrawerAdapter.notifyDataSetChanged();
        drawerList.setOnItemClickListener(this);

    }
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }

    // initialization of the UI components
    public void initialization()
    {
        appointmentFragment = new AppointmentsFragment();
        agentName = (TextView) findViewById(R.id.agentName);
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer, appointmentFragment);
        fragmentTransaction.commit();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerRelative = (RelativeLayout) findViewById(R.id.drawerRelative);
        drawerList = (ListView) findViewById(R.id.drawerList);
    }

    //api call for logout
    void logoutApiCall()
    {
        String token=sharedPreferences.getString(ACCESS_TOKEN, NULL);
        new RestClient().getService().logout(BEARER + token
                , new Callback<ServerResponse>() {
            @Override
            public void success(ServerResponse loginRegisterResponse, Response response) {
                progressDialog.dismiss();
                startActivity(new Intent(AppointmentsActivity.this, SignInActivity.class));
                finish();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                Toast.makeText(AppointmentsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case LOGOUT_BUTTON:
                progressDialog = ProgressDialog.show(this, DIALOG_TITLE, DIALOG_TEXT);
                logoutApiCall();
        }
    }

}

package com.example.arundhati.thereddoor.retrofit;
import com.example.arundhati.thereddoor.retrofit.ApiService;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;


/**
 * Created by Arundhati on 11/2/2015.
 */public class RestClient {
    ApiService service;
    final String BASE_URL = "http://reddoorspa.clicklabs.in:8000";

    public RestClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setClient(new OkClient(okHttpClient))
                .setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter restAdapter = builder.build();
        service = restAdapter.create(ApiService.class);

    }

    public ApiService getService() {
        return service;
    }
}

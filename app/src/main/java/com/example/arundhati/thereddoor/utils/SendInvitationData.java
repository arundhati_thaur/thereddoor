package com.example.arundhati.thereddoor.utils;

import com.example.arundhati.thereddoor.entities.Appointment;

/**
 * Created by Arundhati on 11/10/2015.
 */
public interface SendInvitationData {
    void sendDataToInvitationDetails(Appointment appointmentObject);
}

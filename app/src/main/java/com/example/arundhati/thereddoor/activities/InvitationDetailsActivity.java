package com.example.arundhati.thereddoor.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.arundhati.thereddoor.R;
import com.example.arundhati.thereddoor.entities.Appointment;
import com.example.arundhati.thereddoor.utils.AppConstants;

public class InvitationDetailsActivity extends AppCompatActivity implements View.OnClickListener, AppConstants {

    TextView header, textLocation, customerName, service, duration, location, timeToAccept, dateAndTime;
    Button accept, decline, backButton;
    ImageButton locationOnMap;
    Appointment appointmentObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation_details);
        initialization();
        header.setText(INVITATION_DETAILS);
        textLocation.setText(LOCATION_TEXT);
        locationOnMap.setVisibility(View.GONE);
        appointmentObject = (Appointment) getIntent().getSerializableExtra(INVITATION_OBJECT);
        customerName.setText(appointmentObject.getCustomerName());
        service.setText(appointmentObject.getService());
        location.setText(appointmentObject.getLocation());
        duration.setText(appointmentObject.getDuration());
        dateAndTime.setText(appointmentObject.getTimeOfService() + "," + appointmentObject.getDateOfService());
        timeToAccept.setText(appointmentObject.getTimeToAccept());
        backButton.setOnClickListener(this);
        accept.setOnClickListener(this);
        decline.setOnClickListener(this);

    }

    //initialization of the UI components
    public void initialization() {

        header = (TextView) findViewById(R.id.headerText);
        textLocation = (TextView) findViewById(R.id.textLocation);
        accept = (Button) findViewById(R.id.accept);
        decline = (Button) findViewById(R.id.decline);
        backButton = (Button) findViewById(R.id.backButton);
        customerName = (TextView) findViewById(R.id.customerName);
        service = (TextView) findViewById(R.id.service);
        duration = (TextView) findViewById(R.id.duration);
        location = (TextView) findViewById(R.id.location);
        timeToAccept = (TextView) findViewById(R.id.timeToAccept);
        dateAndTime = (TextView) findViewById(R.id.dateAndTime);
        locationOnMap = (ImageButton) findViewById(R.id.locationOnMap);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backButton:
                super.onBackPressed();
                break;
            case R.id.accept:

                break;
            case R.id.decline:

                break;
        }
    }
}
